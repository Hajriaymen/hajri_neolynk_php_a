<?php

/**
 * Genere un tableau de longueur $length remplit aleatoirement par des nombres entre 0 et 100
 *
 * @param int $length
 *      Longuer de tableau
 * @return array
 *      Tableau d'entier genere aleatoirement de longueur $length
**/
function generateRandomTable($length){
    for ($x = 0; $x < $length; $x++) {
        $randomTable[] = rand(0,100);
    } 
    return $randomTable;
}

/**
 * Donne le nom de la copie locale de la source
 *
 * renvoie le code « true » si et seulement il existe deux nombres 
 * dans le tableau genere aleatoirement dont la somme est strictement 
 * egale au nombre saisi par l’utilisateur. Sinon on renvoie le code « false ». 
 *
 * @param string $randomTable
 *      Tableau d'entier genere aleatoirement
 *        int $n
 *      Nombre recherche entree par l'utilsateur
 * @return boolean
 *      True si somme trouvee trouvee
 *      False si somme non trouvee
**/
function recherche($randomTable, $n){
    $i = 0;
    $j = 1;
    $first = $randomTable[0];
    $second = $randomTable[$j];
    while ($first + $second != $n && $i <= 18){

        if($j == 19){
            $i++;
            $first = $randomTable[$i];
            $j = $i +1;
            $second = $randomTable[$j];
        } else {
            $j++;
            $second = $randomTable[$j];
        }

    }
    if($i > 18){
        return false;
    }
    return true;
}

/**
 * Main program
 *
 * @param string $argc
 *      Nombre de variable passe via ligne de commande
 *        array $argv
 *      Tableau des variables passes via ligne de commande
**/
function main(){
	print "Saissez un nombre entier strictement superieur a 0 et egal ou inferieur a 100 \n";
	$n = filter_var(trim(fgets(STDIN)), FILTER_VALIDATE_INT);
	while(!is_int($n) || $n > 100){
		print "Erreur, vous devez saisir un nombre entier strictement superieur a 0 et egal ou inferieur a 100 \n";
		$n = filter_var(trim(fgets(STDIN)), FILTER_VALIDATE_INT);
	}
	$randomTable = generateRandomTable(20);
	print "Le tableau en question remplit avec 20 valeurs aleatoires comprises entre 0 et 100 est: \n";
	print_r($randomTable);
	//Afin d'optimiser la recherche on tri le tableau
	sort($randomTable);
	$result = recherche($randomTable, $n);
	if($result){
		print "Il existe deux nombres dans le tableau genere aleatoirement dont la somme est strictement egale au nombre saisi \n";
	} else {
		print "Dommage, il n'existe pas deux nombres dans le tableau genere aleatoirement dont la somme est strictement egale au nombre saisi \n";
	}
}

main();
