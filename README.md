# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

On se propose de participer à la création d’un système d’inventaire pour une grande surface. Pour simplifier le travail on admettra qu’il n’y a qu’une seule marque représentée pour chaque produit. Par exemple, il n’existe qu’un et un seul type de dentifrice d’une et une seule marque dans le magasin.
Dans un premier temps la tâche qui nous est confiée consiste à proposer le listing de toutes les références de produits disponibles dans le super marché


Micro projet

Dans le cadre d’un projet nous avons besoin d’une interface accessible en ligne de commande permettant à l’utilisateur de saisir un nombre entier strictement supérieur à 0 et égal ou inférieur à 100. L’application devra renvoyer le code booléen « true » dans certaines conditions et le code « false » dans tous les autres cas.

Au démarrage de l’application on remplit un grand tableau avec 20 valeurs aléatoires comprises entre 0 et 100. Dans un second temps on invite l’utilisateur à saisir son nombre. Ensuite on renvoie le code « true » si et seulement il existe deux nombres dans le tableau généré aléatoirement dont la somme est strictement égale au nombre saisi par l’utilisateur. Sinon on renvoie le code « false ». 
