<?php

class ProductFactory {
  public static function build($product_type, $name, $reference) {
    $product = ucwords($product_type);

    if(class_exists($product))
    {
      return new $product($name, $reference);
    }
    else {
      throw new Exception("Invalid product type given.");
    }
  }
}

class CatalogFactory {
  public static function build($catalogue, $products) {
    $catalogue = ucwords($catalogue);

    if(class_exists($catalogue))
    {
		if(count($products) == 0){
			throw new Exception("You should have at least one product to create a catalog. Invalid products list given. ");
		}
      return new $catalogue($products);
    }
    else {
      throw new Exception("Invalid catalog class name given.");
    }
  }
}

class Product {
   	private $name;
	private $reference;
	
	public function __construct($name, $reference)
    {
        $this->name = $name;
        $this->reference = $reference;
    }
	
	public function __toString() {
        return "Produit: " . $this->getName() . " Reference: " . $this->getReference() . "\n";
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getReference()
    {
        return $this->reference;
    }
}

class Dentifrices extends Product {
	//code spécifique aux Dentifrices
}
class Shampooings extends Product {
	//code spécifique aux Shampooings
}

class Farines extends Product {
	//code spécifique aux Farines
}
 
class Catalogues {
    private $products;
    
    public function __construct($products) {
        $this->products = $products;
    }
    
    public function addProduct(Product $product) {
        $this->products[] = $product;

        return $this;
    }
    
    public function getProducts()
    {
        return $this->products;
    }
}
 
$dentifrice = ProductFactory::build('Dentifrices', 'Colgate', 'P001');
$shampooing = ProductFactory::build('Shampooings', 'Clear', 'P011');
$farine = ProductFactory::build('Farines', 'Farine France', 'P021');
 
$catalogue = CatalogFactory::build('Catalogues', [$dentifrice, $shampooing, $farine]);


$listPrdts = $catalogue->getProducts();
foreach($listPrdts as $p){
    echo $p;
}